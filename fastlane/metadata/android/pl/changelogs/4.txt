Próbuję ponownie ustabilizować zamrażanie. Usunięto nieużywaną funkcję doOnFinished() z FreezerService.
Poprawka regresji: Limit czasu nie jest używany, gdy freezerservice nie jest uruchomiony
